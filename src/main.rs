// Copyright (C) 2023 Davide Peressoni
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use env_logger::Env;
use musescore_video::{create_video::create_video, utils};
use std::{env, ffi::OsStr, path::PathBuf};

const FRAME_RATE: u32 = 60;

fn main() {
    env_logger::Builder::from_env(Env::default().default_filter_or("info")).init();

    let path = env::args().nth(1).expect("Missing source file");
    let file = PathBuf::from(&path);

    if file.is_dir() || file.extension().map(OsStr::to_str) != Some(Some("mscz")) {
        panic!("{} is not a valid Musescore file", &path)
    }

    let mscz = if file.is_relative() {
        env::current_dir().unwrap().join(file)
    } else {
        file
    };

    let media_info = utils::score_media(&mscz).unwrap();
    create_video(media_info, mscz.with_extension("mp4"), FRAME_RATE).unwrap();
}
