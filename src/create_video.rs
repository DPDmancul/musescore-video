// Copyright (C) 2023 Davide Peressoni
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use crate::result::Result;
use crate::utils::ScoreMedia;
use base64::{engine::general_purpose::STANDARD, Engine};
use itertools::Itertools;
use log::{error, info};
use resvg::{
    tiny_skia::Pixmap,
    usvg::{self, Transform, TreeParsing},
};
use serde::{Deserialize, Deserializer};
use std::{
    fmt::Debug, path::Path, process::Command, rc::Rc, str, time::Duration,
};
use tempfile::tempdir;

type Svg = usvg::Tree;

#[derive(Deserialize, Debug)]
struct Score {
    #[serde(deserialize_with = "unwrap_list")]
    elements: Vec<Element>,
    #[serde(deserialize_with = "unwrap_list")]
    events: Vec<Event>,
}

#[derive(Deserialize, Debug, Clone, Copy)]
struct Element {
    #[serde(rename = "@x")]
    x: f32,
    #[serde(rename = "@y")]
    y: f32,
    #[serde(rename = "@sx")]
    sx: f32,
    #[serde(rename = "@sy")]
    sy: f32,
    #[serde(rename = "@page")]
    page: u32,
}

#[derive(Deserialize, Debug)]
struct Event {
    #[serde(rename = "@position")]
    position: u32,
}

struct PosXml {
    position: u32,
    element: Element,
}

struct Keyframe {
    position: u32,
    x: f32,
    system: Element,
}

pub fn create_video(
    media_info: ScoreMedia,
    output: impl AsRef<Path>,
    frame_rate: u32,
) -> Result<()> {
    let svgs = media_info
        .svgs
        .iter()
        .map(parse_svg)
        .collect::<Result<Vec<_>>>()?;
    let keyframes = get_keyframes(media_info)?;
    let dir = tempdir()?;

    for (i, (keyframe, next_keyframe)) in keyframes.iter().tuple_windows().enumerate() {
        let time = i as u32 * 1000 / frame_rate;

        if let Some(keyframe_index) = keyframes
            .iter()
            .rposition(|Keyframe { position, .. }| position <= &time)
        {
            let system = &keyframe.system;
            let svg = &svgs[system.page as usize];
            let x = keyframe.x
                + (time as f32 - keyframe.position as f32)
                    / (next_keyframe.position as f32 - keyframe.position as f32)
                    * (next_keyframe.x - keyframe.x);

            info!(
                "Frame:{} Time:{:.4} Keyframe:{}",
                i,
                time as f32 / 1000.,
                keyframe_index
            );

            let view_box = svg.view_box.rect;
            let view_width = view_box.width();
            let view_height = view_width * 9. / 16.;
            let y_offset =
                keyframe.system.y / 12. + (keyframe.system.sy / 12. - view_height) / 2. + 80.;

            let mut new_svg = svg.clone();

            new_svg.view_box.rect =
                usvg::NonZeroRect::from_xywh(0., y_offset, view_width, view_height)
                    .expect("Zero-sized view");
            new_svg.size = usvg::Size::from_wh(1080., 720.).expect("Zero-sized svg");

            let mut rect = usvg::Path::new(Rc::new(usvg::tiny_skia_path::PathBuilder::from_rect(
                usvg::tiny_skia_path::Rect::from_xywh(0., y_offset, view_width, view_height)
                    .expect("Zero-size view"),
            )));
            rect.fill = Some(usvg::Fill::from_paint(usvg::Paint::Color(
                usvg::Color::white(),
            )));
            new_svg
                .root
                .prepend(usvg::Node::new(usvg::NodeKind::Path(rect)));

            let mut line_builder = usvg::tiny_skia_path::PathBuilder::new();
            line_builder.move_to(x / 12., system.x / 12. - 55.);
            line_builder.line_to(x / 12., system.y / 12. + system.sy / 12. + 55.);
            if let Some(line_path) = line_builder.clone().finish() {
                let mut line = usvg::Path::new(Rc::new(line_path));
                line.stroke = Some(usvg::Stroke {
                    width: usvg::NonZeroPositiveF32::new(6.).unwrap(),
                    paint: usvg::Paint::Color(usvg::Color::new_rgb(69, 160, 209)),
                    ..usvg::Stroke::default()
                });
                new_svg
                    .root
                    .append(usvg::Node::new(usvg::NodeKind::Path(line)));
            } else {
                error!("Cannot build line from {:?}", line_builder);
            }

            let svg_tree = resvg::Tree::from_usvg(&new_svg);
            let pixmap_size = svg_tree.size.to_int_size();
            let mut pixmap = Pixmap::new(pixmap_size.width(), pixmap_size.height()).unwrap();
            svg_tree.render(Transform::default(), &mut pixmap.as_mut());
            pixmap.save_png(dir.path().join(format!("{i:06}.png"))).unwrap();
        } else {
            break;
        }
    }

    Command::new("ffmpeg")
        .arg("-y")
        .args(["-framerate", &frame_rate.to_string()])
        .args(["-i", "%06d.png"])
        .arg(output.as_ref().to_string_lossy().as_ref())
        .current_dir(dir.path())
        .spawn()?;

    Ok(())
}

fn get_keyframes(media_info: ScoreMedia) -> Result<Vec<Keyframe>> {
    let mut mpos = parse_pos_xml(&media_info.mpos_xml)?;
    let spos = parse_pos_xml(&media_info.spos_xml)?;

    mpos.push(PosXml {
        position: media_info.metadata.duration * 1000,
        element: Element {
            x: 0.,
            y: 0.,
            sx: 0.,
            sy: 0.,
            page: 0,
        },
    });

    struct Measure<'a> {
        start: u32,
        end: u32,
        element: &'a Element,
    }

    let measures = mpos.windows(2).map(|pos| Measure {
        start: pos[0].position,
        end: pos[1].position,
        element: &pos[0].element,
    });

    Ok(measures
        .into_group_map_by(|m| m.element.page)
        .iter()
        .flat_map(|(&page, measures)| {
            let x = measures
                .iter()
                .map(|m| m.element.x)
                .fold(f32::INFINITY, f32::min);
            let y = measures
                .iter()
                .map(|m| m.element.y)
                .fold(f32::INFINITY, f32::min);

            let system = Element {
                x,
                y,
                sx: measures
                    .iter()
                    .map(|m| m.element.x + m.element.sx)
                    .fold(f32::NEG_INFINITY, f32::max)
                    - x,
                sy: measures
                    .iter()
                    .map(|m| m.element.y + m.element.sy)
                    .fold(f32::NEG_INFINITY, f32::max)
                    - y,
                page,
            };

            measures
                .iter()
                .flat_map(
                    |Measure {
                         start,
                         end,
                         element: Element { x, sx, .. },
                         ..
                     }| {
                        spos.iter()
                            .filter(|PosXml { position, .. }| position >= start && position <= end)
                            .map(
                                |PosXml {
                                     position,
                                     element: Element { x, .. },
                                     ..
                                 }| Keyframe {
                                    position: *position,
                                    x: *x,
                                    system,
                                },
                            )
                            .chain(vec![Keyframe {
                                position: *end,
                                x: x + sx,
                                system,
                            }])
                    },
                )
                .collect::<Vec<_>>()
        })
        .collect())
}

fn parse_pos_xml(pos_xml: impl AsRef<[u8]>) -> Result<Vec<PosXml>> {
    let Score {
        elements, events, ..
    } = quick_xml::de::from_str(&esab64(pos_xml)?)?;

    Ok(events
        .into_iter()
        .zip(elements)
        .map(|(Event { position, .. }, element)| PosXml { position, element })
        .collect())
}

fn parse_svg(svg: impl AsRef<[u8]>) -> Result<Svg> {
    Ok(Svg::from_str(&esab64(svg)?, &usvg::Options::default())?)
}

fn esab64(base64: impl AsRef<[u8]>) -> Result<String> {
    Ok(str::from_utf8(&STANDARD.decode(base64)?)?.to_owned())
}

fn unwrap_list<'a, D, T>(deserializer: D) -> Result<Vec<T>, D::Error>
where
    D: Deserializer<'a>,
    T: Deserialize<'a> + Debug,
{
    #[derive(Deserialize, Debug)]
    struct List<T> {
        #[serde(rename = "$value")]
        content: Vec<T>,
    }

    Ok(List::<T>::deserialize(deserializer)?.content)
}
