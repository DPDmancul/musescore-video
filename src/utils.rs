// Copyright (C) 2023 Davide Peressoni
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

use crate::result::*;
use serde::Deserialize;
use std::ffi::OsStr;
use std::path::{Path, PathBuf};
use std::process::Command;
use std::str::{self, FromStr};
use tempfile::{tempdir, TempDir};

#[derive(Deserialize, Debug)]
pub struct ScoreMedia {
    pub svgs: Vec<String>,
    #[serde(rename = "mposXML")]
    pub mpos_xml: String,
    #[serde(rename = "sposXML")]
    pub spos_xml: String,
    pub metadata: Metadata,
}

#[derive(Deserialize, Debug)]
pub struct Metadata {
    pub duration: u32,
}

pub struct TempFile {
    pub path: PathBuf,
    _dir: TempDir,
}

pub fn get_audio_length(file: impl AsRef<Path>) -> Result<u32> {
    Ok(u32::from_str(str::from_utf8(
        &Command::new("ffprobe")
            .args(["-i", &file.as_ref().to_string_lossy()])
            .args(["-show_entries", "format=duration"])
            .args(["-v", "quiet"])
            .args(["-of", "csv=p=0"])
            .output()?
            .stdout,
    )?)?)
}

pub fn score_media(file: impl AsRef<Path>) -> Result<ScoreMedia> {
    Ok(serde_json::from_str(str::from_utf8(
        &Command::new("mscore")
            .args(["--score-media", &file.as_ref().to_string_lossy()])
            .output()?
            .stdout,
    )?)?)
}

pub fn score_export(file: impl AsRef<Path>, ext: impl AsRef<OsStr>) -> Result<TempFile> {
    let dir = tempdir()?;
    let path = dir
        .path()
        .with_file_name(
            file.as_ref()
                .file_name()
                .unwrap_or_else(|| OsStr::new("output")),
        )
        .with_extension(ext);

    Command::new("mscore")
        .args(["--export-to", &path.to_string_lossy()])
        .arg(file.as_ref().to_string_lossy().as_ref())
        .spawn()?;

    Ok(TempFile { path, _dir: dir })
}
