# Musescore video

Export videos from musescore files.

Inspired by [score-util](https://github.com/keijokapp/score-util/tree/master).
